from fastapi import FastAPI

from . import routers

from .db import database

app = FastAPI()
app.include_router(routers.router)
sleep_time = 10

@app.on_event("startup")
async def startup():
    await database.connect()

@app.on_event("shutdown")
async def shutdown():
    await database.disconnect()