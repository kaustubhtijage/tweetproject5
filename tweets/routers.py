from fastapi import APIRouter

from .crud import get_user_tweet,get_all_tweets,insert_tweet

from .schemas import Tweet

router = APIRouter()

@router.get("/tweets")
async def read_tweets():
    tweets_list = await get_all_tweets()
    return tweets_list

@router.get("/user_tweet")
async def user_tweet(user_id:int):
    tweet = await get_user_tweet(user_id)
    return tweet

@router.post("/Insert_Tweet")
async def in_tweet(tweets:Tweet):
    inserted_tweet = await insert_tweet(tweets)
    return inserted_tweet