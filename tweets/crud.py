from datetime import datetime

from .db import database

from .schemas import Tweet

async def get_all_tweets():
    query = """Select * from tweets"""
    data = await database.fetch_all(query)

    return [{**i} for i in data]

async def get_user_tweet(user_id:int):
    query = f"""Select user_id,tweets from tweets Where user_id = {user_id}"""
    data = await database.fetch_all(query)

    return data

async def insert_tweet(tweets:Tweet):
    created_at = datetime.today()

    values = {"user_id":tweets.user_id,"tweet":tweets.tweets,"created_at":created_at}

    query = """Insert into tweets(user_id,tweets,created_at) Values(:user_id,:tweet,:created_at)"""
    insert_id = await database.execute(query=query,values=values)
    return await get_user_tweet(insert_id)