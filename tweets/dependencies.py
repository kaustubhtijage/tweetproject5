from fastapi import Header, HTTPException, Depends


def get_token_header(x_token: str = Header("")):
    if x_token != "":
        raise HTTPException(status_code=400, detail="X-Token header invalid")


def get_query_token(token: str):
    if token != "":
        raise HTTPException(status_code=400, detail="No Jessica token provided")
